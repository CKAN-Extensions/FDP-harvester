#!/bin/bash
# Let's call this by venv
echo "Running /usr/lib/ckan/default/src/ckanext-dcat/ckanext/dcat/f2ds_harvester.py (with virtualenv enabled, using python from /usr/lib/ckan/default/bin/python)"
/usr/lib/ckan/default/bin/python /usr/lib/ckan/default/src/ckanext-dcat/ckanext/dcat/f2ds_harvester.py &

exit 0
